create database library;

use library;

create table books(
id bigint unsigned primary key,
title varchar(100) not null,
author_id bigint unsigned not null,
genre varchar(40) not null,
content longtext not null,
pages_number bigint unsigned not null,
note_id bigint unsigned not null,
comment_id bigint unsigned not null,
book_status tinyint default 0,
foreign key (author_id) references authors(id),
foreign key (note_id) references notes(id),
foreign key (comment_id) references comments(id)
);

create table notes(
id bigint unsigned primary key,
page_number bigint unsigned not null,
note varchar(100) not null
);

create table comments(
id bigint unsigned primary key,
comment varchar(500) not null
);

create table authors(
id bigint unsigned primary key, 
name varchar(255) not null,
email varchar(40) null default null,
address varchar(100) null default null
);

create table users(
id bigint unsigned primary key,
username varchar(40) not null,
password varchar(40) not null,
firstname varchar(40) not null,
lastname varchar(40) not null,
email varchar(40) not null
);

create table followers(
follower_id bigint unsigned,
followed_id bigint unsigned,
primary key (follower_id, followed_id),
foreign key (follower_id) references users(id),
foreign key (followed_id) references users(id)
);

create table users_books(
user_id bigint unsigned not null,
book_id bigint unsigned not null,
primary key(user_id, book_id),
foreign key(user_id) references users(id),
foreign key(book_id) references books(id)
);