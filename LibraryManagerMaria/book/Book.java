package lm.LibraryManager.book;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Book {

	private long bookId;
	private String title;
	private String author;
	private String genre;
	private int numberOfPages;
	private int yearPublished;
	private String cover;
	private int rating = 1;
	private ArrayList<Comment> comments;
	
	public Book(){
		
	}
	
	public Book(long bookId, String title, String author, String genre, int numberOfPages, 
			int yearPublished, String cover){
		
		this.bookId = bookId;
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.numberOfPages = numberOfPages;
		this.yearPublished = yearPublished;
		this.cover = cover;
		
		this.comments = new ArrayList<Comment>();
	}

	public void addComment(Comment comment){
		comments.add(comment);
	}

	public long getBookId() {
		return bookId;
	}

	public void setBookId(long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public int getYearPublished() {
		return yearPublished;
	}

	public void setYearPublished(int yearPublished) {
		this.yearPublished = yearPublished;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}
	
	@Override
	public String toString(){
		String bookDesc =  "id: " + bookId + "; title: " + title + "; author: " +
				author + "; genre: " + genre + "; number pages: " + numberOfPages + "; year: " +
				yearPublished + "; cover: " + cover + "; comments: ";
		for(Comment com : comments){
			bookDesc = bookDesc + com + ", ";
		}
		
		return bookDesc;
	}
	
}
