package lm.LibraryManager.book;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlRootElement;

import lm.LibraryManager.user.User;


@XmlRootElement
public class Comment {

	private User user;
	private String comment;
	private LocalDate commentDate;
	
	public Comment(){
		
	}
	
	public Comment(User user, String comment){
		this.user = user;
		this.comment = comment;
		
		this.commentDate = LocalDate.now();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDate getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(LocalDate commentDate) {
		this.commentDate = commentDate;
	}
	
	@Override
	public String toString(){
		return comment;
	}
}
