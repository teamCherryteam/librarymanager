package lm.LibraryManager.user;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {

	private static int idCounter = 1;
	
	private int id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	
	public User(){
		
	}
	public User(String username, String password, String firstName, String lastName, String email){
		
		id = idCounter;
		idCounter++;
		if(username != null && username != "")
			this.username = username;
		if(password != null && password != "")
			this.password = password;
		if(firstName != null && firstName != "")
			this.firstName = firstName;
		if(lastName != null && lastName != "")
			this.lastName = lastName;
		if(email != null && email != "")
			this.email = email;		
	}

	public int getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}
}
