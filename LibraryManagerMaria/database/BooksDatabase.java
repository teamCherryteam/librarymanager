package lm.LibraryManager.database;


import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

import lm.LibraryManager.book.Book;

@XmlRootElement
public class BooksDatabase {

	//HashMap< /bookId/ Long, Book>
	private static HashMap<Long, Book> allBooks = new HashMap<Long, Book>();
	
	static{
		allBooks.put(1L, new Book(1L, "A Dark Lure", "Loreth Anne White", "Comedy",
				345, 2015, "cover1"));
		allBooks.put(2L, new Book(2L, "Wait for Signs", " George Guidall", 
				"Comedy", 563, 2013, "cover2"));
		allBooks.put(3L, new Book(3L, "Luckiest Girl Alive: A Novel", "Jessica Knoll", 
				"Fantsy", 126, 2009, "cover3"));
		allBooks.put(4L, new Book(4L, "The Last Town", "Blake Crouch", 
				"Fantasy", 233, 1996, "cover4"));
		allBooks.put(5L, new Book(5L, "Darkness Brutal (The Dark Cycle)", "Rachel A. Marks", 
				"Action", 675, 2010, "cover5"));
	}

	public BooksDatabase(){
		
	}
	
	public static HashMap<Long, Book> getAllBooks() {
		return allBooks;
	}
	
	
}
