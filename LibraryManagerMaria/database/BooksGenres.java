package lm.LibraryManager.database;


import java.util.HashMap;
import java.util.HashSet;

import javax.xml.bind.annotation.XmlRootElement;

import lm.LibraryManager.book.Book;



@XmlRootElement
public class BooksGenres {

	private static HashMap<String, HashSet<Book>> books = 
			new HashMap<String, HashSet<Book>>();
	
	static{
		books.put("Comedy", new HashSet<Book>());
		books.get("Comedy").add(new Book(1L, "A Dark Lure", "Loreth Anne White", 
				"Comedy", 345, 2015, "cover1"));
		books.get("Comedy").add(new Book(2L, "Wait for Signs", " George Guidall", 
				"Comedy", 563, 2013, "cover2"));
		
		books.put("Fantasy", new HashSet<Book>());
		books.get("Fantasy").add(new Book(3L, "Luckiest Girl Alive: A Novel", "Jessica Knoll", 
				"Fantasy", 126, 2009, "cover3"));
		books.get("Fantasy").add(new Book(4L, "The Last Town", "Blake Crouch", 
				"Fantasy", 233, 1996, "cover4"));
		
		books.put("Action", new HashSet<Book>());
		books.get("Action").add(new Book(5L, "Darkness Brutal (The Dark Cycle)", "Rachel A. Marks", 
				"Action", 675, 2010, "cover5"));
	}

	public BooksGenres(){
		
	}
	public static HashMap<String, HashSet<Book>> getBooks() {
		return books;
	}
	
	
}
