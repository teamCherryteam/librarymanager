package lm.LibraryManager.database;


import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

import lm.LibraryManager.user.User;


@XmlRootElement
public class UsersDatabase {

	private static HashMap<Long, User> allUsers = new HashMap<Long, User>();

	static {
		allUsers.put(1L, new User("username1", "0001", "Eleonora",
				"Ivanova", "eivanova@gmail.com"));
		allUsers.put(2L, new User("username2", "0002", "Jack",
				"Samuel", "jsamuel@gmail.com"));
		allUsers.put(3L, new User("username3", "0003", "Emily",
				"Candrich", "ecandrich@gmail.com"));
		allUsers.put(4L, new User("username4", "0004", "Sami",
				"Roayl", "sroyal@gmail.com"));
		allUsers.put(5L, new User("username5", "0005", "Daniela",
				"Georgieva", "dgeorgieva@gmail.com"));
	}

	public UsersDatabase(){
		
	}
	
	public static HashMap<Long, User> getAllUsers() {
		return allUsers;
	}
}
