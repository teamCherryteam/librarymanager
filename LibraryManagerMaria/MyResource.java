package lm.LibraryManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lm.LibraryManager.book.Book;
import lm.LibraryManager.book.Comment;
import lm.LibraryManager.database.BooksDatabase;
import lm.LibraryManager.database.BooksGenres;
import lm.LibraryManager.database.UsersDatabase;
import lm.LibraryManager.user.User;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("library")
public class MyResource {

	private HashMap<Long, Book> allBooks = BooksDatabase.getAllBooks();
	private HashMap<Long, User> allUsers = UsersDatabase.getAllUsers(); 
	private HashMap<String, HashSet<Book>> booksByGenre = BooksGenres.getBooks();
	
	private long timeInMillis = 0;
	
	public void printBooks(){
		for(Entry<Long, Book> entry : allBooks.entrySet()){
			System.out.println(entry.getKey() + " = " + entry.getValue());
			System.out.println("-----------------------------------------");
		}
	}
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }
    
    @GET
	@Path("/addcomment")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addBookComment(@QueryParam("userId") long userId,
								   @QueryParam("bookId") long bookId,
								   @QueryParam("rating") int rating,
								   @QueryParam("comment") String comment){
		
		if(!allBooks.containsKey(bookId) || !allUsers.containsKey(userId)){
			return Response.status(Response.Status.BAD_REQUEST).
					entity("Not added").build();
		}
		else{
			
			Book book = allBooks.get(bookId);
			book.setRating(rating);
			
			User user = allUsers.get(userId);
			
			book.addComment(new Comment(user, comment));
			System.out.println("Book: " + book);
			printBooks();
			return Response.status(Response.Status.OK).
					entity("Comment added").build();
		}
	}
    
    @POST
    @Path("/addnewbook")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewBook(Book book){
			
		if(allBooks.containsKey(book.getBookId())){
			return Response.status(Response.Status.BAD_REQUEST).
					entity("Bad request").build();
		}
		else{
			
			//All genres will be in database in advance
			if(booksByGenre.containsKey(book.getGenre())){
				booksByGenre.get(book.getGenre()).add(book);
				
				allBooks.put(book.getBookId(), book);
				System.out.println("Book: " + book);
				printBooks();
				return Response.status(Response.Status.OK).
						entity("OK,insert complete").build();
			}
			
			return Response.status(Response.Status.BAD_REQUEST).
					entity("Bad request").build();
		}
		
	}
    
    @GET
	@Path("/ratebook/{bookId}/{rating}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rateBook(@PathParam("bookId") long bookId,
						     @PathParam("rating") int rating){
		
		if(!allBooks.containsKey((long)bookId)){
			return Response.status(Response.Status.BAD_REQUEST).
					entity("Not added").build();
		}
		else{
			
			Book book = allBooks.get((long)bookId);
			book.setRating(rating);
			System.out.println("Book: " + book);
			printBooks();
			return Response.status(Response.Status.OK).
					entity("Rating added").build();
		}
	}
    
    @GET
    @Path("/starttimer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response startTimer(){
    	timeInMillis = System.currentTimeMillis();
    	if(timeInMillis != 0){
    		return Response.status(Response.Status.OK).
    				entity("Timer started").build();
    	}
    	else{
    		return Response.status(Response.Status.BAD_REQUEST).
    				entity("Not started").build();
    	}
    	
    }
    
    @GET
    @Path("/stoptimer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response stopTimer(){
    	timeInMillis = System.currentTimeMillis() - timeInMillis;
    	if(timeInMillis != 0){
    		return Response.status(Response.Status.OK).
    				entity("{timeInMillis : " + timeInMillis + "}").build();
    	}
    	else{
    		return Response.status(Response.Status.BAD_REQUEST).
    				entity("Error").build();
    	}
    	
    }
    
    @GET
	@Path("/booksbygenre/{genre}")
	@Produces(MediaType.APPLICATION_JSON)
	public HashSet<Book> searchBooksByGenre(@PathParam("genre") String genre) throws IOException{
		
		if(booksByGenre.containsKey(genre)){
			printBooks();
			return booksByGenre.get(genre);
		}
		else{
			IOException e = new IOException();
			throw e;
		}
	}
    
    @GET
	@Path("/viewbook/{bookId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book viewBook(@PathParam("bookId") int bookId) throws IOException{
		
		for(Entry<Long, Book> entry : allBooks.entrySet()){
			System.out.println(entry.getKey() + " : " + entry.getValue().getTitle());
		}
		System.out.println("Book id " + bookId);
		System.out.println(allBooks.containsKey(bookId));
		System.out.println(allBooks.get((long) bookId).getTitle());
		System.out.println(allBooks.containsKey((long)bookId));
		if(!allBooks.containsKey((long)bookId)){
			System.out.println("ssss");
			IOException e = new IOException();
			throw e;
		}
		else{
			System.out.println("Book: " + allBooks.get((long) bookId));
			printBooks();
			System.out.println("ssss");
			return allBooks.get((long) bookId);
		}
	
	}
}
